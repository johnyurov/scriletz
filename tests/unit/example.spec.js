import { mount } from '@vue/test-utils'
import About from '@/views/About.vue'

describe('About.vue', () => {
  const wrapper = mount(About)
  it('initial search value', () => {
    expect(wrapper.vm.search).toBe('')
  })
  it('initial busy value', () => {
    expect(wrapper.vm.busy).toBe(false)
  })
  it('initial timeout', () => {
    expect(wrapper.vm.timeout).toBe(null)
  })
  it('input value not equal search', () => {
    const testInput = wrapper.find('input')
    testInput.element.value = 'Clementine Bauch'
    let search = wrapper.vm.search
    search = 'Leanne Graham'
    expect(testInput.element.value).not.toEqual(search)
  })
  it('dataTest value equal empty or search value', () => {
    let searchUser = wrapper.vm.dataTest
    let search = wrapper.vm.search
    search = 'Leanne Graham'
    expect(searchUser).toEqual([])
    searchUser.push('Leanne Graham')
    expect(searchUser[0]).toEqual(search)
    searchUser.splice(0, searchUser.length)
  })
  it('input trigger not equal search', () => {
    const testInput = wrapper.find('input')
    testInput.trigger('input')
    expect(wrapper.vm.search).not.toEqual('')
  })
})
